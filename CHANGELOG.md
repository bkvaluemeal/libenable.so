# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## 0.3.0 - Xvfb
### Added
- xvfbwrapper module
- Headless operation

### Removed
- supervisord from Docker image

## 0.2.1 - reCAPTCHA url
### Fixed
- reCAPTCHA route

## [0.2.0] - reCAPTCHA
### Added
- reCAPTCHA route

## [0.1.0] - Web interface
### Added
- Web interface
- Command line application
- RESTful API
- Configuration manager
- Activity log

[0.2.0]: https://bitbucket.org/bkvaluemeal/libenable.so/issues/2/recaptcha
[0.1.0]: https://bitbucket.org/bkvaluemeal/libenable.so/issues/1/web-interface
